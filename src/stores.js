import { writable } from 'svelte/store';

function createRoomList() {
	const { subscribe, set, update } = writable(["_default_room"]);

	return {
		subscribe,
		roomstore: (val) => set(val),
		reset: () => set(0)
	};
}

export const roomlist = createRoomList();